Whether you’re a local looking to find the best flower, concentrates, edibles, and accessories in town, or a tourist seeking out the best that Colorado’s marijuana industry has to offer, LivWell Trinidad is the perfect recreational marijuana dispensary for you.

Address: 124 Santa Fe Trail, Trinidad, CO 81082, USA

Phone: 719-422-8251

Website: https://www.livwell.com/trinidad-marijuana-dispensary
